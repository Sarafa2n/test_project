from django.db import models


class Question(models.Model):
    title = models.CharField(verbose_name="заголовок", max_length=255)
    text = models.TextField()
    created_dt = models.DateTimeField()


class Answer(models.Model):
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
